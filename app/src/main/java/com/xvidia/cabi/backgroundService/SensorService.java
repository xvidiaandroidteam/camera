package com.xvidia.cabi.backgroundService;



import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.provider.Settings.Secure;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.xvidia.cabi.data.AcceleroGyroData;
import com.xvidia.cabi.data.LightData;
import com.xvidia.cabi.data.LocationData;
import com.xvidia.cabi.data.PressureData;
import com.xvidia.cabi.data.ProximityData;
import com.xvidia.cabi.data.TemperatureData;
import com.xvidia.cabi.network.IAPIConstants;
import com.xvidia.cabi.network.ServiceURLManager;
import com.xvidia.cabi.network.VolleySingleton;
import com.xvidia.cabi.service.MyApplication;

import org.java_websocket.client.WebSocketClient;
import org.java_websocket.drafts.Draft_17;
import org.java_websocket.handshake.ServerHandshake;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Date;
import java.util.Vector;


public class SensorService extends Service implements LocationListener,SensorEventListener {
    private static final String DEBUG_TAG = "mWebSocketClient";
 

	private LocationManager mLocationManager = null;
	private Criteria criteria;
	private String provider; 
	private SensorManager sensorManager = null;
    private Sensor sensor = null;
    public static String android_id;
    static WebSocketClient mWebSocketClient;
    static boolean  timerRunning; //isConnected,
    static int TIMER_TIME= 1*1000;
    static int TIMER_TIME_2SEC= 2*1000;
    Vector<Float> lightData,pressureData;
    static long startTime, stopTime;
    static boolean stopTimeFlag;
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
    	setAndroidId();
    	connectWebSocket();
        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
//		if (sensorManager.getDefaultSensor(Sensor.TYPE_LIGHT) != null) {
//			sensor = sensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);
//			sensorManager.registerListener(this, sensor, SensorManager.SENSOR_DELAY_NORMAL);
//		}
		if (sensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE) != null) {
			sensor = sensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
			sensorManager.registerListener(this, sensor, SensorManager.SENSOR_DELAY_NORMAL);
		}
		if (sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER) != null) {
			sensor = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
			sensorManager.registerListener(this, sensor, SensorManager.SENSOR_DELAY_NORMAL);
		} 
//		if(sensorManager.getDefaultSensor(Sensor.TYPE_PRESSURE) != null){
//			sensor = sensorManager.getDefaultSensor(Sensor.TYPE_PRESSURE);
//	        sensorManager.registerListener(this, sensor,SensorManager.SENSOR_DELAY_NORMAL);
//		}
//		if(sensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY) != null){
//			sensor = sensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY);
//	        sensorManager.registerListener(this, sensor,SensorManager.SENSOR_DELAY_FASTEST);
//		}
//		if(sensorManager.getDefaultSensor(Sensor.TYPE_AMBIENT_TEMPERATURE) != null){
//			sensor = sensorManager.getDefaultSensor(Sensor.TYPE_AMBIENT_TEMPERATURE);
//	        sensorManager.registerListener(this, sensor,SensorManager.SENSOR_DELAY_NORMAL);
//		}
    	initializeLocationManager();
		
        return START_STICKY;
    }
 
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
 
    private void setAndroidId(){
    	android_id = Secure.getString(getApplicationContext().getContentResolver(),Secure.ANDROID_ID);
    	android_id = "Cam1";
    	
    }
//    private boolean isNetworkAvailable() {
//        ConnectivityManager connectivityManager 
//              = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
//        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
//        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
//    }
    private void connectWebSocket() {
        URI uri;
        try{
        if(mWebSocketClient != null){
        	mWebSocketClient.close();
        }
        }catch(Exception e){
        	Log.i("Websocket", "Onclose error");
        }
        mWebSocketClient = null;
        try {
            uri = new URI("ws://192.168.1.14:8080/sensor");
//            uri = new URI("ws://officecam.selfip.com:8080/sensor");
        } catch (URISyntaxException e) {
            e.printStackTrace();
            return;
        }
        try{
	        mWebSocketClient = new WebSocketClient(uri, new Draft_17()) {
	            @Override
	            public void onOpen(ServerHandshake serverHandshake) {
	                Log.i("Websocket", "Opened");
//	                isConnected = true;
//	                count = 1;
	               
	            }
	
	            @Override
	            public void onMessage(String s) {
	                Log.i("Websocket", "onMessage Response " + s);
	            }
	
	            @Override
	            public void onClose(int i, String s, boolean b) {
	                Log.i("Websocket", "Closed " + s);
//	                isConnected = false;
//	                count = 0;
	            }
	
	            @Override
	            public void onError(Exception e) {
	                Log.e("Websocket", "Error " + e.getMessage());
	            }
	
				
	        };
	        mWebSocketClient.connect();
		}catch(Exception e){
			Log.e("Websocket", "Error " + e.getMessage());
		}
    }
    /**
	 * In this method Location services is initialised
	 */
	private void initializeLocationManager(){
		provider = mLocationManager.getBestProvider(criteria, false);
		Location location = mLocationManager.getLastKnownLocation(provider);
		// getting GPS status
		boolean isGPSEnabled = mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

		// getting network status
		boolean isNetworkEnabled = mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

		if (!isGPSEnabled && !isNetworkEnabled) {
			Toast.makeText(this, "Location service failed. Enable Location Service", Toast.LENGTH_LONG).show();
		} else {
			// First get location from Network Provider
			if (isNetworkEnabled) {
				if (mLocationManager != null) {
					mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, TIMER_TIME_2SEC, 1, this);
					location = mLocationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
					if (location != null) {
						onLocationChanged(location);
					}
				}
			}else{
				Toast.makeText(this, "No Location service Network", Toast.LENGTH_LONG).show();
			}
			//get the location by gps
			if (isGPSEnabled) {
				if (location == null) {
					if (mLocationManager != null) {
						mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,TIMER_TIME_2SEC, 1, this);
						Toast.makeText(this, "Location service GPS", Toast.LENGTH_LONG).show();
						location = mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
						if (location != null) {
							onLocationChanged(location);
						}
					}
				}
			} else {
				Toast.makeText(this, "no Location service GPS", Toast.LENGTH_LONG).show();
			}
		}
//		// Get the location manager
//		  mLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
//		  // Define the criteria how to select the location provider
//		  criteria = new Criteria();
//		  criteria.setAccuracy(Criteria.ACCURACY_COARSE);	//default
//
//		  criteria.setCostAllowed(false);
//		  // get the best provider depending on the criteria
//		  provider = mLocationManager.getBestProvider(criteria, false);
	    
		  // the last known location of this provider
//		  Location location = mLocationManager.getLastKnownLocation(provider);
//
//
//		  if (location != null) {
//			  onLocationChanged(location);
//		  } else {
//			  // leads to the settings because there is no last known location
////			  Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
////			  startActivity(intent);
//		  }		  mLocationManager.requestLocationUpdates(provider, TIMER_TIME_2SEC, 1, this);
		  // location updates: at least 1 meter and 200millsecs change
	
	        
		  
	}
	


	@Override
	public void onLocationChanged(Location location) {
		  float lat =(float) location.getLatitude();
		  float longitude = (float) location.getLongitude();
		  float altitude = (float) location.getAltitude();
		  float speed = location.getSpeed();
		  Date nw = new Date();
		  long time = nw.getTime();
		  if(!stopTimeFlag && speed <=1){
			  stopTimeFlag = true;
			  startTime = time;
			  new Handler().postDelayed(new Runnable() {
					
					@Override
					public void run() {
						if(!stopTimeFlag){
							startTime = 0;
							stopTime =0;
						}
					}
				}, 2000);
		  }else if(stopTimeFlag && speed >1){
			  stopTimeFlag = false;
			  stopTime = time;
		  }
		 
			Log.i("onLocation Service", "onLocationChanged "+speed);
//		  if(isConnected){
//		    	sendMessage(getJSONLocation(time,lat,longitude,altitude,speed));
//		  }else if(count == 0&& !isConnected){
//			 connectWebSocket(); 
//		  }else{
		sendLocationRequest(time, lat, longitude, altitude, speed);
//			  if(mWebSocketClient.getReadyState()== WebSocket.READY_STATE_CLOSED){
//				  connectWebSocket();
//			  }else if(mWebSocketClient.getReadyState()== WebSocket.READY_STATE_CONNECTING){
//				  Log.i(DEBUG_TAG, "Wsclient message READY_STATE_CONNECTING");
//			  }else if(mWebSocketClient.getReadyState()== WebSocket.READY_STATE_OPEN){
////				  sendMessage(getJSONLocation(time, lat, longitude, altitude, speed));
//			  }
			 
//		  }
    	
   
	}

	@Override
	public void onProviderDisabled(String arg0) {
		
	}

	@Override
	public void onProviderEnabled(String arg0) {
		
	}

	@Override
	public void onStatusChanged(String arg0, int arg1, Bundle arg2) {
		
	}

	/**
	 * Checks internet connectivity
	 * @return
	 */
	boolean checkNetwrk(){
		boolean nwFlag = false;
		try{
			ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
			if (networkInfo != null && networkInfo.isConnected()) {
				nwFlag = true;
			}

		}catch (Exception e) {
		}

		return nwFlag;
	}

	/**
	 * Sends http request to server
	 * @param jsonRequest
	 * @param api
	 */
	private void sendPostRequest(String jsonRequest, int api){
		if(jsonRequest != null){

			if(checkNetwrk()){
				final String url = ServiceURLManager.getInstance().getUrl(api);

				JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonRequest, new Response.Listener<JSONObject>() {

					@Override
					public void onResponse(JSONObject response) {

						Log.i("Volley JSONObject ", response.toString());

					}
				}, new Response.ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError error) {
						if(error != null) {
							NetworkResponse networkResponse = error.networkResponse;
							Log.i("Volley error service ", error.getMessage());
						}
					}
				});
				VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
			}
		}
	}

	/**
	 * Sends Location data
	 * @param timeStamp
	 * @param latitude
	 * @param longitude
	 * @param altitude
	 * @param speed
	 */
	public void sendLocationRequest(long timeStamp, float latitude,float longitude,float altitude, float speed) {
		if(android_id.isEmpty()){
			setAndroidId();
		}
		JSONObject jsonRequest = null;
		ObjectMapper mapper = new ObjectMapper();
		String jsonRequestString = null;
		LocationData locationData = new LocationData();
		locationData.setSensorId("GPS");
		locationData.setDeviceId(android_id);
		locationData.setLat(latitude);
		locationData.setLongitude(longitude);
		locationData.setAltitude(altitude);
		locationData.setSpeed(speed);
		locationData.setTimeStamp(timeStamp);
		try {
			jsonRequestString = mapper.writeValueAsString(locationData);
			Log.i("VolleyLayoutTimeData ", jsonRequestString.toString());
		} catch (IOException e) {
		}


		sendPostRequest(jsonRequestString,1);
	}
	
	@Override
	public void onSensorChanged(SensorEvent event) {
		if (event != null) {
			startTimer();
			long timeStamp = event.timestamp;
			switch (event.sensor.getType()) {

			case Sensor.TYPE_ACCELEROMETER:
				float xAxis = event.values[0];
				float yAxis = event.values[1];
				float zAxis = event.values[2];
//				if (isConnected){
//					sendMessage(getJSONAccelerometer(timeStamp, xAxis, yAxis,
//							zAxis));
//				}else if(count == 0&& !isConnected){
//					 connectWebSocket(); 
//				  }
//				if (mWebSocketClient != null) {
//					if (mWebSocketClient.getReadyState() == WebSocket.READY_STATE_CLOSED) {
//						connectWebSocket();
//					} else if (mWebSocketClient.getReadyState() == WebSocket.READY_STATE_CONNECTING) {
//						Log.i(DEBUG_TAG, "Wsclient message READY_STATE_CONNECTING");
//					} else if (mWebSocketClient.getReadyState() == WebSocket.READY_STATE_OPEN) {
//
//					}
//
//				}
				sendAccelerometerRequest(timeStamp, xAxis, yAxis, zAxis);
				break;
			case Sensor.TYPE_LIGHT:
				float light = event.values[0];
				if(lightData!= null){
					lightData.add(light);
				}else{
					lightData = new Vector<Float>();
					lightData.add(light);
				}
//				if (isConnected){
//					sendMessage(getJSONLight(timeStamp, light));
//				}else if(count == 0&& !isConnected){
//					 connectWebSocket(); 
//				  }
//				if (mWebSocketClient != null) {
//					if (mWebSocketClient.getReadyState() == WebSocket.READY_STATE_CLOSED) {
//						connectWebSocket();
//					} else if (mWebSocketClient.getReadyState() == WebSocket.READY_STATE_CONNECTING) {
//						Log.i(DEBUG_TAG, "Wsclient message READY_STATE_CONNECTING");
//					} else if (mWebSocketClient.getReadyState() == WebSocket.READY_STATE_OPEN) {
//						sendMessage(getJSONLight(timeStamp,light));
//					}
//
//				}
				break;
			case Sensor.TYPE_GYROSCOPE:
				float xGAxis = event.values[0];
				float yGAxis = event.values[1];
				float zGAxis = event.values[2];
//				if (isConnected){
//					sendMessage(getJSONGyroscope(timeStamp, xGAxis, yGAxis,
//							zGAxis));
//				}else if(count == 0&& !isConnected){
//					 connectWebSocket(); 
//				  }else{
//				if (mWebSocketClient != null) {
//					if (mWebSocketClient.getReadyState() == WebSocket.READY_STATE_CLOSED) {
//						connectWebSocket();
//					} else if (mWebSocketClient.getReadyState() == WebSocket.READY_STATE_CONNECTING) {
//						Log.i(DEBUG_TAG, "Wsclient message READY_STATE_CONNECTING");
//					} else if (mWebSocketClient.getReadyState() == WebSocket.READY_STATE_OPEN) {
//						sendMessage(getJSONGyroscope(timeStamp, xGAxis, yGAxis,
//								zGAxis));
//					}
//
//				}
				sendGyroscopeRequest(timeStamp, xGAxis, yGAxis,zGAxis);
				break;
			case Sensor.TYPE_PRESSURE:
				float dataPressure = event.values[0];
//				if (isConnected){
//					sendMessage(getJSONPressure(timeStamp, dataPressure));
//				}else if(count == 0&& !isConnected){
//					 connectWebSocket(); 
//				  }
//				if (mWebSocketClient != null) {
//					if (mWebSocketClient.getReadyState() == WebSocket.READY_STATE_CLOSED) {
//						connectWebSocket();
//					} else if (mWebSocketClient.getReadyState() == WebSocket.READY_STATE_CONNECTING) {
//						Log.i(DEBUG_TAG, "Wsclient message READY_STATE_CONNECTING");
//					} else if (mWebSocketClient.getReadyState() == WebSocket.READY_STATE_OPEN) {
//						sendMessage(getJSONPressure(timeStamp, dataPressure));
//					}
//
//				}
				sendPressureRequest(timeStamp, dataPressure);
				break;
			case Sensor.TYPE_PROXIMITY:
				float dataProximity = event.values[0];
//				if (isConnected){
//					sendMessage(getJSONProximity(timeStamp, dataProximity));
//				}else if(count == 0&& !isConnected){
//					 connectWebSocket(); 
//				  }
//				if (mWebSocketClient != null) {
//					if (mWebSocketClient.getReadyState() == WebSocket.READY_STATE_CLOSED) {
//						connectWebSocket();
//					} else if (mWebSocketClient.getReadyState() == WebSocket.READY_STATE_CONNECTING) {
//						Log.i(DEBUG_TAG, "Wsclient message READY_STATE_CONNECTING");
//					} else if (mWebSocketClient.getReadyState() == WebSocket.READY_STATE_OPEN) {
//						sendMessage(getJSONProximity(timeStamp, dataProximity));
//					}
//
//				}
				sendProximityRequest(timeStamp, dataProximity);
				break;
			case Sensor.TYPE_AMBIENT_TEMPERATURE:
				float dataTemp = event.values[0];
//				if (isConnected){
//					sendMessage(getJSONTemperature(timeStamp, dataTemp));
//				}else if(count == 0&& !isConnected){
//					 connectWebSocket(); 
//				  }
//				if (mWebSocketClient != null) {
//					if (mWebSocketClient.getReadyState() == WebSocket.READY_STATE_CLOSED) {
//						connectWebSocket();
//					} else if (mWebSocketClient.getReadyState() == WebSocket.READY_STATE_CONNECTING) {
//						Log.i(DEBUG_TAG, "Wsclient message READY_STATE_CONNECTING");
//					} else if (mWebSocketClient.getReadyState() == WebSocket.READY_STATE_OPEN) {
//						sendMessage(getJSONTemperature(timeStamp, dataTemp));
//					}
//
//				}
				sendTemperatureRequest(timeStamp, dataTemp);
				break;
			}

		}

	}

	private void startTimer(){
		if(!timerRunning){
			timerRunning = true;
			new Handler().postDelayed(new Runnable() {
				
				@Override
				public void run() {
					Date nw = new Date();
					  long time = nw.getTime();
					  float light = calculateAverage(lightData);
//					  if (mWebSocketClient != null) {
//							if (mWebSocketClient.getReadyState() == WebSocket.READY_STATE_CLOSED) {
//								connectWebSocket();
//							} else if (mWebSocketClient.getReadyState() == WebSocket.READY_STATE_CONNECTING) {
////								if(!isNetworkAvailable()){
////									mWebSocketClient.close();
////									Log.i(DEBUG_TAG, "Wsclient message CLOSEDDD");
////								}
//								Log.i(DEBUG_TAG, "Wsclient message READY_STATE_CONNECTING");
//							} else if (mWebSocketClient.getReadyState() == WebSocket.READY_STATE_OPEN) {
////								sendMessage(getJSONLight(time,light));
//							}
//
//						}
					sendLightRequest(time,light);
					lightData = new Vector<Float>();
					timerRunning = false;
					Log.e("LIGHT DATA IS SENT", "Wsclient Light data -->> "+light);
				}
			}, TIMER_TIME);
		}
	}
	
	private float calculateAverage(Vector<Float> dataVector){
		float average = 0.0f;
		if(dataVector!=null &&dataVector.size()>0){
			float tempVal = average;
			for(int i =0; i<dataVector.size();i++){
				tempVal = tempVal+dataVector.elementAt(i);
			}
			average = tempVal/dataVector.size();
		}
		return average;
	}
	public static void sendMessage(String message) {
    	try{
    	 mWebSocketClient.send(message);
//    	 Log.i(DEBUG_TAG, "Wsclient message "+message);
    	}catch(IllegalStateException e){
    		Log.e(DEBUG_TAG, "Wsclient error message "+e.getMessage());
    	}catch(Exception e){
    		Log.e(DEBUG_TAG, "Wsclient error message "+e.getMessage());
    	}
    }
	


	/**
	 * Sends ambient light data
	 * @param timeStamp
	 * @param light
	 */
	private void sendLightRequest(long timeStamp, float light) {
		JSONObject jsonObject = new JSONObject();
		try {
			if(android_id.isEmpty()){
				setAndroidId();
			}
			JSONObject jsonRequest = null;
			ObjectMapper mapper = new ObjectMapper();
			String jsonRequestString = null;
			LightData obdLightData = new LightData();
			obdLightData.setSensorId("TYPE_LIGHT");
			obdLightData.setCameraId(android_id);
			obdLightData.setLight(light);
			obdLightData.setTimeStamp(timeStamp);
			try {
				jsonRequestString = mapper.writeValueAsString(obdLightData);
				Log.i("VolleyLayoutTimeData ", jsonRequestString.toString());
			} catch (IOException e) {
			}
			sendPostRequest(jsonRequestString, IAPIConstants.API_KEY_OBD_COOLANT_TEMPERATURE);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * Sends engine Pressure
	 * @param timeStamp
	 * @param pressure
	 */
	private void sendPressureRequest(long timeStamp, float pressure) {
		JSONObject jsonObject = new JSONObject();
		try {
			if(android_id.isEmpty()){
				setAndroidId();
			}
			JSONObject jsonRequest = null;
			ObjectMapper mapper = new ObjectMapper();
			String jsonRequestString = null;
			PressureData obdPressureData = new PressureData();
			obdPressureData.setSensorId("TYPE_PRESSURE");
			obdPressureData.setCameraId(android_id);
			obdPressureData.setPressure(pressure);
			obdPressureData.setTimeStamp(timeStamp);
			try {
				jsonRequestString = mapper.writeValueAsString(obdPressureData);
				Log.i("VolleyLayoutTimeData ", jsonRequestString.toString());
			} catch (IOException e) {
			}
			sendPostRequest(jsonRequestString, IAPIConstants.API_KEY_OBD_COOLANT_TEMPERATURE);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * Sends object proximity with camera data
	 * @param timeStamp
	 * @param proximity
	 */
	private void sendProximityRequest(long timeStamp, float proximity) {
		JSONObject jsonObject = new JSONObject();
		try {
			if(android_id.isEmpty()){
				setAndroidId();
			}
			JSONObject jsonRequest = null;
			ObjectMapper mapper = new ObjectMapper();
			String jsonRequestString = null;
			ProximityData obdProximityData = new ProximityData();
			obdProximityData.setSensorId("TYPE_PROXIMITY");
			obdProximityData.setCameraId(android_id);
			obdProximityData.setProximity(proximity);
			obdProximityData.setTimeStamp(timeStamp);
			try {
				jsonRequestString = mapper.writeValueAsString(obdProximityData);
				Log.i("VolleyLayoutTimeData ", jsonRequestString.toString());
			} catch (IOException e) {
			}
			sendPostRequest(jsonRequestString, IAPIConstants.API_KEY_OBD_COOLANT_TEMPERATURE);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * Sends engine temperature data
	 * @param timeStamp
	 * @param temperature
	 */
	private void sendTemperatureRequest(long timeStamp, float temperature) {
		JSONObject jsonObject = new JSONObject();
		try {
			if(android_id.isEmpty()){
				setAndroidId();
			}
			JSONObject jsonRequest = null;
			ObjectMapper mapper = new ObjectMapper();
			String jsonRequestString = null;
			TemperatureData obdTemperatureData = new TemperatureData();
			obdTemperatureData.setSensorId("TYPE_AMBIENT_TEMPERATURE");
			obdTemperatureData.setCameraId(android_id);
			obdTemperatureData.setTemperature(temperature);
			obdTemperatureData.setTimeStamp(timeStamp);
			try {
				jsonRequestString = mapper.writeValueAsString(obdTemperatureData);
				Log.i("VolleyLayoutTimeData ", jsonRequestString.toString());
			} catch (IOException e) {
			}
			sendPostRequest(jsonRequestString, IAPIConstants.API_KEY_OBD_COOLANT_TEMPERATURE);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * sends Accelerometer data
	 * @param timeStamp
	 * @param xAxis
	 * @param yAxis
	 * @param zAxis
	 */
	private void sendAccelerometerRequest(long timeStamp, float xAxis,float yAxis, float zAxis) {
  		JSONObject jsonObject = new JSONObject();
  		try {
  			if(android_id.isEmpty()){
				setAndroidId();
			}
			JSONObject jsonRequest = null;
			ObjectMapper mapper = new ObjectMapper();
			String jsonRequestString = null;
			AcceleroGyroData accelerometerData = new AcceleroGyroData();
			accelerometerData.setSensorId("TYPE_ACCELEROMETER");
			accelerometerData.setCameraId(android_id);
			accelerometerData.setAxisX(xAxis);
			accelerometerData.setAxisY(yAxis);
			accelerometerData.setAxisZ(zAxis);
			accelerometerData.setTimeStamp(timeStamp);
			try {
				jsonRequestString = mapper.writeValueAsString(accelerometerData);
				Log.i("VolleyLayoutTimeData ", jsonRequestString.toString());
			} catch (IOException e) {
			}
			sendPostRequest(jsonRequestString, IAPIConstants.API_KEY_OBD_COOLANT_TEMPERATURE);

  		} catch (Exception e) {
  			e.printStackTrace();
  		}

  	}

	/**
	 * Sends Gyroscope data
	 * @param timeStamp
	 * @param xAxis
	 * @param yAxis
	 * @param zAxis
	 */
	private void sendGyroscopeRequest(long timeStamp, float xAxis,float yAxis, float zAxis) {
		JSONObject jsonObject = new JSONObject();
		try {
			if(android_id.isEmpty()){
				setAndroidId();
			}
			JSONObject jsonRequest = null;
			ObjectMapper mapper = new ObjectMapper();
			String jsonRequestString = null;
			AcceleroGyroData gyroData = new AcceleroGyroData();
			gyroData.setSensorId("TYPE_GYROSCOPE");
			gyroData.setCameraId(android_id);
			gyroData.setAxisX(xAxis);
			gyroData.setAxisY(yAxis);
			gyroData.setAxisZ(zAxis);
			gyroData.setTimeStamp(timeStamp);
			try {
				jsonRequestString = mapper.writeValueAsString(gyroData);
				Log.i("VolleyLayoutTimeData ", jsonRequestString.toString());
			} catch (IOException e) {
			}

			sendPostRequest(jsonRequestString, IAPIConstants.API_KEY_OBD_COOLANT_TEMPERATURE);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	@Override
	public void onAccuracyChanged(Sensor arg0, int arg1) {
		
	}
}