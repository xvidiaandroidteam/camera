package com.xvidia.cabi.broadcastReceiver;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;

import com.xvidia.cabi.backgroundService.UploadOfflineVideoTask;
import com.xvidia.cabi.utils.AppUtil;
import com.xvidia.cabi.utils.NetworkUtil;
/**
 * This class extends {@link BroadcastReceiver} to update nework change information
 * @author Ravi@xvidia
 * @since Version 1.0
 */
public class NetworkChangeReceiver extends BroadcastReceiver {
 
    @Override
    public void onReceive(final Context context, final Intent intent) {

		 try{
			String status = NetworkUtil.getConnectivityStatusString(context);
			if (!status.equalsIgnoreCase("Not connected to Internet")) {
				try {
					sendLogRequest(context);
				} catch (Exception e) {

				}
			}
		 }catch(Exception e){
			 
		 }
    }
    
    private void sendLogRequest(Context ctx){
  	  try {
  		  if(AppUtil.checkNetwrk()){
  				new UploadOfflineVideoTask(ctx).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, (Void)null);
	  		}
  		}catch(Exception e){
  			
  		}
  	}
   }