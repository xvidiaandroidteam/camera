package com.xvidia.cabi.network.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by vasu on 5/5/16.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class CreateSession {

    @JsonProperty("apiKey")
    String apiKey;
    @JsonProperty("sessionId")
    String sessionId;

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getCarRegNo() {
        return carRegNo;
    }

    public void setCarRegNo(String carRegNo) {
        this.carRegNo = carRegNo;
    }

    @JsonProperty("token")
    String token;
    @JsonProperty("carRegNo")
    String carRegNo;
}
